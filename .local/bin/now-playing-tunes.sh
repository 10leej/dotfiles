dbus-monitor --session "path=/org/mpris/MediaPlayer2" --monitor | awk '
  /string  *"xesam:artist/{
    while (1) {
      getline line
      if (line ~ /string "/){
        sub(/.*string "/, "Artist: ", line)
        sub(/".*$/, "", line)
        print line > "/home/joshua/Documents/Stream/song-artist.txt"
        close("/home/joshua/Documents/Stream/song-artist.txt")
        break
      }
    }
  }
  /string  *"xesam:title/{
    while (1) {
      getline line
      if (line ~ /string "/){
        sub(/.*string "/, "Title: ", line)
        sub(/".*$/, "", line)
        print line > "/home/joshua/Documents/Stream/song-title.txt"
        close("/home/joshua/Documents/Stream/song-title.txt")
        break
      }
    }
  }
'